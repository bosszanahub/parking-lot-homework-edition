# #!/usr/bin/env ruby
require 'rspec'
require './functional_spec/lib/parking_lot'

def rspec_test
    return RSpec::Core::Runner.run([File.dirname(__FILE__) + '/specs'])
end

def main
    #before start process use Rspec Test ==> Sohuld Move to sh script
    #rspec_test
    #check input file send via parameter sh.
    p = "initiate_command"
    unless ARGV.length == 0
        # input_file = File.read('./functional_spec/fixtures/file_input.txt')
        #should include correct machine file
        begin
            input_file = File.read(ARGV[0])
            #open loop command
            input_file.split("\n").each do |input|
                seperator_input = input.split(/\s/)
                cmd = seperator_input.first
                # puts "#{seperator_input.count}"
                if cmd == "create_parking_lot"
                    p = ParkingLot.new(seperator_input[1])
                elsif cmd == "leave"
                    p.send(cmd, "#{seperator_input[1]}")
                elsif cmd == "status"
                    p.send(cmd)
                else
                    p.send(cmd, *([seperator_input[1],seperator_input[2]]).compact)
                end
            end
        rescue => exception
            puts exception
        end
    else
        puts "Please attach file"
    end
end

main()