require_relative "./car"

class Slot
    attr_accessor :id, :car

    def initialize(id)
        @id = id.to_i
    end

    def assign_slot(car_no, car_color)
        if self.car
            "Full"
        else
            self.car = Car.new(car_no, car_color)
        end
    end

    def leave
        self.car = nil
    end

    def is_space?
        self.car.nil?
    end

    def car_no
        car.car_no || car
    end

    def car_color
        car.car_color || car
    end
end