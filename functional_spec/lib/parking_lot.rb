require_relative "./slot"

class ParkingLot

    INITITATE_SLOT = 2

    attr_accessor :slots

    def initialize(slots=INITITATE_SLOT)
        @slots = []
        slots.to_i.times.map { |s| @slots << Slot.new(s + 1)}
        puts "Create a parking lot with #{@slots.count} slots"
    end

    #start input command
    def status
        puts "Slot No.\t\t Registration No\t\t Colour\n"
        @slots.each do |slot|
            slot_car = slot.car
            unless slot_car.nil?
                puts "#{slot.id}\t\t\t #{slot_car.car_no}\t\t\t #{slot_car.car_color}"
            end
        end
    end

    def park(car_no, car_color)
        if set_slot
            puts "Allocated slot number: #{set_slot.id}"
            set_slot.assign_slot(car_no,car_color)
        else
            puts "Sorry, parking lot is full" 
        end
    end

    def leave(slot_no)
        slot_number = slot_no.to_i
        if slot_number.to_i > 0 && slot_number <= @slots.length
            # slot = @slots.find { |e| e.id == slot_number}
            slot = @slots[slot_number - 1]
            slot.leave
            puts "Slot number #{slot_number} is free" 
        else
            puts "You try to select leave slot #{slot_number}, unfortunately we have #{@slots.length}"
        end
    end

    def registration_numbers_for_cars_with_colour(color)
        slots = []
        @slots.select { |slot|  slot.car.car_color == color ? slots.push(slot.car.car_no) : "" }
        puts slots.count > 0 ? slots.join(',') : "Not found"
    end
    
    def slot_numbers_for_cars_with_colour(color)
        slots = []
        @slots.select { |slot| slot.car.car_color == color ? slots.push(slot.id) : "" }
        puts slots.count > 0 ? slots.join(',') : "Not found"
    end

    def slot_number_for_registration_number(car_no)
        # puts "car #{car_no}"
        slots = []
        @slots.select { |slot| slot.car.car_no == car_no ? slots.push(slot.id) : ""}
        puts slots.count > 0 ? slots.join(',') : "Not found"
    end

    #end of command

    private 

    def set_slot
        @slots.find do |slot|
            slot.is_space?
        end
    end
end