class Car
    attr_accessor :car_no, :car_color

    def initialize(car_no, car_color)
        @car_no = car_no
        @car_color = car_color
    end
end