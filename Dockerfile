FROM ruby:2.5.1-alpine

ENV BUNDLER_VERSION=2.0.2

RUN apk update && apk add --no-cache bash

RUN gem install bundler -v 2.0.2

RUN mkdir /app

WORKDIR /app

COPY functional_spec/Gemfile ./
COPY functional_spec/Gemfile.lock ./

RUN bundle install

COPY . .

CMD ["sh", "bin/setup"]